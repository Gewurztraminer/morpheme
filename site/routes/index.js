var express = require('express');
var router = express.Router();
var fs = require('fs');
var net = require('net');
var ws = require('nodejs-websocket');
var util = require('./routesutilities.js');

var backupInput = [];
var intervalInLoop = false;

var rooms = { 
	dictionary: 0, 
	percentageaccepted: 80, 
	boarddimensions: 6, 
	mimimumwordlength: 6, 
	minimumwordcount: 85, 
	bonuswordgreaterthan: 8, 
	bonuswordlessthan: 13, 
	freq: [1000, 319, 439, 300, 600, 208, 230, 333, 900, 28, 194, 540, 388, 530, 760, 402, 28, 650, 480, 540, 475, 139, 166, 41, 250, 55] 
};


//52.34.206.59
var client = new net.Socket();

function reload(){

var server = ws.createServer(function(conn) {

	console.log('New WebSocket connection.');


	conn.on("text", function (str) {
		console.log("Received " +str);
		if (str === 'close'){
		client.destroy();
		}
	});

	conn.on("close", function (code, reason) {
		console.log("WebSocket connection closed.");
		client.destroy(); 
	
	});


setTimeout(function(){ client.connect(6780, '127.0.0.1', function() {
		console.log('Connected to Java server.');
	});

	client.on('data', function(data) {
   conn.sendText(data.toString());
		if (data.toString() === 'sclose~`~'){
			client.destroy();
		} 

		
	});

	client.on("text", function (str) {
		console.log("Received " +str);
	});
	
	client.on('close', function() {
		console.log('Closing connection to Java server.');
	});
var sendableString = "sload board" + "~`~" + util.convertToSendableString(rooms);
		client.write(sendableString);  }, 3000);

	
}).listen(8080);
};

reload();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Gewurztraminer.', condition: false });
});

/* GET users listing. */
router.get('/users', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/users/detail', function(req, res, next) {
  res.send('detail');
});


module.exports = router;
