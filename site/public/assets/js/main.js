
start();

function start(websocketServerLocation){

	var something = document.getElementById('BoardPanelScript');
	var currentSplitURL = window.location.toString().split("/");
	var websocketServerLocation = 'ws://' + currentSplitURL[2].split(':')[0] + ':8080';
	var connection = new WebSocket(websocketServerLocation);

   	connection.onmessage = function (event) {
		var inputData = event.data.toString();
		var inputObject = parseInput(inputData);
		var identifier = inputObject[0];
		console.log(identifier);
		if (identifier != 'close'){

			if (identifier === 'load board'){
				changeBoardLetters(inputObject[1]);
			}

		} else connection.close();
	};

	connection.onopen = function(event) {
		console.log('WebSocket connection opened.');
	};
   
	connection.onclose = function(event) {
		 

		console.log('WebSocket connection closed.');
	};      

	window.onbeforeunload = function(e) {
	  	var dialogText = 'Dialog text here';
	  	e.returnValue = dialogText;
		connection.send('close');
	  	return dialogText;
	};
}


	
