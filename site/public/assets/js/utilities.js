function parseInput(string){

	var splitString = string.split("~`~");
	var returnObject = [];

	for (i = 0; i < splitString.length; i++){


		var identifier = splitString[i].substring(0, 1);
		splitString[i] = splitString[i].substring(1, splitString[i].length);

		switch(identifier) {
		    case "s":
			returnObject.push(splitString[i]);
			break;
		    case "i":
			returnObject.push(parseInt(splitString[i]));
			break;
		    case "S":
			returnObject.push(splitString[i].split("`"));
			break;  
		    case "I":
			var strObj = splitString[i].split("`");
			var intObj = {};
			for (q = 0; q < strObj.length; q++){
				intObj[intObj.length] = parseInt(strObj[q]);
			}
			returnObject.push(intObj);
			break;
		}
	}

	return returnObject;

}
