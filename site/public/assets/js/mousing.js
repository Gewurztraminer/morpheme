var mouseIsDown = false;
var lettersSelected = [];

var xIndex;
var yIndex;





	$("#room").mouseup(function() {
		if (mouseIsDown){
		lettersSelected = [];
		repaint();
		$( "#board-panel" ).trigger( "mouseup" );
		}
	});

	$("#board-panel")
	.mousedown(function() {
		mouseIsDown = true;
		var the = xAndYCoordinates();
		xIndex = Math.floor(the.X / (sizeOfImage + df));
		yIndex = Math.floor(the.Y / (sizeOfImage + df));
		if (the.X >= 0 && the.Y >= 0 && xIndex >= 0 && yIndex >= 0){
			document.getElementById("wordsubmission").value = boardLetters2D[yIndex][xIndex];
			var hash = (xIndex * 100) + yIndex;
			lettersSelected[0] = hash;
			repaint();
		}
	})

	.mousemove(function( event ) {

		if (mouseIsDown === true){
			var the = xAndYCoordinates();
			xIndex = Math.floor(the.X / (sizeOfImage + df));
			yIndex = Math.floor(the.Y / (sizeOfImage + df));	
			var cellOffsetX = Math.floor(the.X % (sizeOfImage + df));
			var cellOffsetY = Math.floor(the.Y % (sizeOfImage + df));
			var fractionOfImageSize = (sizeOfImage * (34 / 100)) + (boardDimensions - 3);
			var hash = (xIndex * 100) + yIndex;

			if (cellOffsetX <= sizeOfImage && cellOffsetY <= sizeOfImage) {

				if (cellOffsetX < fractionOfImageSize && cellOffsetY < fractionOfImageSize){
					if (cellOffsetX + cellOffsetY >= fractionOfImageSize){
						addHashToLettersSelectedAndRepaint(hash);
					}
				}

				if (cellOffsetX > fractionOfImageSize && cellOffsetY < fractionOfImageSize){
					if ((sizeOfImage - cellOffsetX) + cellOffsetY >= fractionOfImageSize){
						addHashToLettersSelectedAndRepaint(hash);
					}
				}

				if (cellOffsetX < fractionOfImageSize && cellOffsetY > fractionOfImageSize){
					if (cellOffsetX + (sizeOfImage - cellOffsetY) >= fractionOfImageSize){
						addHashToLettersSelectedAndRepaint(hash);
					}
				}

				if (cellOffsetX > fractionOfImageSize && cellOffsetY > fractionOfImageSize){
					if ((sizeOfImage - cellOffsetX) + (sizeOfImage - cellOffsetY) >= fractionOfImageSize){
						addHashToLettersSelectedAndRepaint(hash);
					}
				}

			}
		}

	})

	.mouseup(function() {
		mouseIsDown = false;
		lettersSelected = [];
		document.getElementById("wordsubmission").value = '';
		repaint();
	});







function lettersSelectedContains(hash){

var isContained = false;

for (let x = 0; x < lettersSelected.length; x++){
	if (lettersSelected[x] === hash){
		isContained = true;
		break;
	}
}

return isContained;

}


function addHashToLettersSelectedAndRepaint(hash){
	if (!lettersSelectedContains(hash)){
		lettersSelected[lettersSelected.length] = hash;	
		document.getElementById("wordsubmission").value = document.getElementById("wordsubmission").value + boardLetters2D[yIndex][xIndex];	
		repaint();
	} else {
		if (lettersSelected.length > 1){
			if (lettersSelected[lettersSelected.length - 2] === hash){
				lettersSelected.splice(-1,1);
				document.getElementById("wordsubmission").value = document.getElementById("wordsubmission").value.substring(0, document.getElementById("wordsubmission").value.length - 1);
				repaint();			
			}
		}
	}
}

function xAndYCoordinates(){

	//startX and startY are the locations at which the div "room" begins.
	var t;
	$('#board-panel').each(function(){
	    t = $(this);
	    t.startX = t.position().left;
	    t.startY = t.position().top;
	});

	//mouseX and mouseY are the locations on the entire browser window at which the mouse event has occurred.
	var mouseX = event.clientX;
	var mouseY = event.clientY;

	return { X: (mouseX - t.startX), Y: (mouseY - t.startY) };

}


