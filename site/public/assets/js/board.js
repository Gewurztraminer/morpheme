var boardLetters = "";
var boardPanelSize = document.getElementById('board-panel').offsetWidth;
var boardDimensions = 0;
var marginPercentage = '1';
var df = (marginPercentage / 100) * boardPanelSize;
var sizeOfImage = (boardPanelSize / boardDimensions) - df;
var boardLetters2D = [[]];

function changeBoardLetters(newLetters) {

	boardLetters = newLetters.toUpperCase();	
	boardDimensions = Math.sqrt(boardLetters.length);	
	sizeOfImage = (boardPanelSize / boardDimensions) - df;	
	boardLetters2D = new Array(boardDimensions);
	for(let y = 0; y < boardDimensions; y++){
		boardLetters2D[y] = new Array(boardDimensions);
		for(let z = 0; z < boardDimensions ; z++){

			var index = (y * boardDimensions) + z;
			boardLetters2D[y][z] = boardLetters.substring((y * boardDimensions) + z, (y * boardDimensions) + z + 1);
		
		}
	}	
	repaint();

};

function repaint(lettersSelected){

	$('#board-panel #letters').html('');

	var c = ((sizeOfImage / boardPanelSize) * 100) ;
	var f = c.toString();
	var content = '';

	for (let x = 0; x < boardDimensions; x++){
		for (let u = 0; u < boardDimensions; u++){

			var tileString = '';
			var hash = (u * 100) + x;
			if (lettersSelectedContains(hash)){
				tileString = 'wordgamecubedark';
			} else {
				tileString = 'wordgamecubenormal';
			}

			if (x === boardDimensions - 1 || u === boardDimensions - 1){
				if (!(x === boardDimensions - 1 && u === boardDimensions - 1)) {

					if (x === boardDimensions - 1) {
						content = content + '<img src="/assets/js/tiles/' + tileString + 
						boardLetters2D[x][u] + '.png" ondragstart="return false;" style="float: left; width: ' + f 
						+ '%; margin-right: 1%;" />';
					}
					if (u === boardDimensions - 1) {
						content = content + '<img src="/assets/js/tiles/' + tileString + 
						boardLetters2D[x][u] + '.png" ondragstart="return false;" style="float: left; width: ' + f 
						+ '%; margin-bottom: 1%;" />';
					}
				
				} else {
					content = content + '<img src="/assets/js/tiles/' + tileString +
					boardLetters2D[x][u] + '.png" ondragstart="return false;" style="float: left; width: ' + f 
					+ '%;" />';
				}
			} else {
				content = content + '<img src="/assets/js/tiles/' + tileString +
				boardLetters2D[x][u] + '.png" ondragstart="return false;" style="float: left; width: ' + f 
				+ '%; margin-right: 1%; margin-bottom: 1%;" />';
			}

		}
	}

	$('#board-panel #letters').html(content);

}

function getStyleRuleValue(style, selector) {
var selector_compare=selector.toLowerCase();
var selector_compare2= selector_compare.substr(0,1)==='.' ?  selector_compare.substr(1) : '.'+selector_compare;

for (var i = 0; i < document.styleSheets.length; i++) {
    var mysheet = document.styleSheets[i];
    var myrules = mysheet.cssRules ? mysheet.cssRules : mysheet.rules;
    for (var j = 0; j < myrules.length; j++) {
        if (myrules[j].selectorText) {
            var check = myrules[j].selectorText.toLowerCase();
            switch (check) {
                case selector_compare  :
                case selector_compare2 : return myrules[j].style[style];
            }
        }
    }

}
};

