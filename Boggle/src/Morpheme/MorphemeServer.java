package Morpheme;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import javax.swing.Timer;
import java.util.ArrayList;
import java.util.Hashtable;

public class MorphemeServer {
	
	public static boolean canWeUsePersonalizedWordList = false;	
	
	public ServerSocket serverSocket;
	public Socket socket;
	
	public static ArrayList<MorphemeClientThread> threads = new ArrayList<MorphemeClientThread>();
	public static ArrayList<Socket> sockets = new ArrayList<Socket>();
	
	public static String[] dictionaryFileNames = { "TWL", "CSW" };
	public static ArrayList<ArrayList<String>> dictionaryArrayLists = new ArrayList<ArrayList<String>>();
	public static ArrayList<Hashtable<String, Integer>> dictWordHashTables = new ArrayList<Hashtable<String, Integer>>();
	public static ArrayList<Hashtable<String, Integer>> potentialWordHashTables = new ArrayList<Hashtable<String, Integer>>();
	public static ArrayList<ArrayList<ArrayList<String>>> letterLengthGrouping = new ArrayList<ArrayList<ArrayList<String>>>();
	public static ArrayList<String> definitions;
	public static Hashtable<String, Integer> personalWords;
	public static Hashtable<String, Integer> v7;
	public static Hashtable<String, Integer> g7;
	public static Hashtable<String, Integer> h7;
	public static Hashtable<String, Integer> n7;
	public static Hashtable<String, Integer> v6;
	public static Hashtable<String, Integer> g6;
	public static Hashtable<String, Integer> h6;
	public static Hashtable<String, Integer> n6;
	
	public MorphemeServer(){
				
		//ArrayList<String> ar = new ArrayList<String>();
		//ar.add("56");
		//Database db = new Database();
		//db.overwrite("aloha", "id", ar);
		
		initArraysAndHashes();
		
		new routineCheckForDisconnectedUsers();
		
		try {
			serverSocket = new ServerSocket(6780, 100);
		} catch (IOException e){
			e.printStackTrace();
		}
		
	while (true) {

		try {
				
			socket = serverSocket.accept();	

			int f = 0;
			boolean torf = false;
			int ID = 0;
			ArrayList<Integer> iDS = new ArrayList<Integer>();
			for (int i = 0; i < MorphemeServer.threads.size(); i++){
				iDS.add(MorphemeServer.threads.get(i).ID);
			}
			do {
				torf = false;
				f++;	
				if (iDS.contains(f)){
					torf = true;
				}	
			} while (torf == true);
			ID = f;
			
			MorphemeClientThread clientThread = new MorphemeClientThread(socket);
			threads.add(clientThread);
			clientThread.start();
			
		} catch (IOException e){
			e.printStackTrace();
		}
		}
			
	}
	
	
	private void initArraysAndHashes(){
		
		System.out.println("gewurztraminer.");
		
		DictionaryInit di = new DictionaryInit();
		
		for (int h = 0; h < dictionaryFileNames.length; h++){
			
			dictionaryArrayLists.add(new ArrayList<String>(di.addDictionary(dictionaryFileNames[h] + ".txt", canWeUsePersonalizedWordList)));
			potentialWordHashTables.add(new Hashtable<String, Integer>(di.potentialWordArrayListToHashTable(dictionaryArrayLists.get(h))));
			dictWordHashTables.add(new Hashtable<String, Integer>(di.dictArrayListToHashTable(dictionaryArrayLists.get(h))));
			letterLengthGrouping.add(new ArrayList<ArrayList<String>>(di.letterLengthGrouping(dictionaryArrayLists.get(h))));
		}	
		
		definitions = new ArrayList<String>(di.definitions());
		personalWords = (Hashtable<String, Integer>) di.addPersonalWords(new Hashtable<String, Integer>());
		v7 = di.vghn67("v7.txt", new Hashtable<String, Integer>());
		g7 = di.vghn67("g7.txt", new Hashtable<String, Integer>());
		h7 = di.vghn67("h7.txt", new Hashtable<String, Integer>());
		n7 = di.vghn67("n7.txt", new Hashtable<String, Integer>());
		v6 = di.vghn67("v6.txt", new Hashtable<String, Integer>());
		g6 = di.vghn67("g6.txt", new Hashtable<String, Integer>());
		h6 = di.vghn67("h6.txt", new Hashtable<String, Integer>());
		n6 = di.vghn67("n6.txt", new Hashtable<String, Integer>());
	}
	
	public static void sysout(String string){
		System.out.println(string);
	}
	
	public static void main(String[] args){
		new MorphemeServer();
	}
	

}

class routineCheckForDisconnectedUsers{
	
	public routineCheckForDisconnectedUsers(){
		Timer timer = new Timer(500, new TimerListener());
		timer.start();
	}
	
	private class TimerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			for (int x = 0; x < MorphemeServer.threads.size(); x++){
			
				MorphemeServer.threads.get(x).send("sare you dead~`~");
			
				
			}
			
		}
		
	}
	
}