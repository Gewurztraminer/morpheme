package Morpheme;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Database {
	
	public String[] fileNames = { "id", "password", "rating", "joindate" };
	public String[] values = new String[fileNames.length];
	
	public File mainFile = new File("users");
	public String dirname = mainFile.getAbsolutePath();

	public Database(){

	}
	
	public String addUser(String username, String password){
		
		String returnString = "";
		File userFile = new File(dirname + "/" + username);
	
        if (!userFile.exists()) {
            if (userFile.mkdir()) {

            	for (int i = 0; i < fileNames.length; i++){
        			File file = new File(dirname + "/" + username + "/" + fileNames[i] + ".txt");
        			if (!file.exists()) {
        				try {
							file.createNewFile();
						} catch (IOException e) {
							returnString += "Error creating " + fileNames[i] + " .txt.";
							e.printStackTrace();
						}
        			}

            	String write = "";
            		
            		if (fileNames[i].equals("id")){
            			write = getSize(mainFile);
            		} else
            			if (fileNames[i].equals("password")){
                			write = password;
                		} else
                			if (fileNames[i].equals("rating")){
                    			write = "1200";
                    		} else
                    			if (fileNames[i].equals("joindate")){
                    				DateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");
                    				Date date = new Date();
                    				write = dateFormat.format(date).toString();
                        		} else
                        			if (fileNames[i].equals("")){
                            			
                            		}
            		
            	
            	
            		FileWriter fw;
					try {
						fw = new FileWriter(file.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
        			char[] content = write.toCharArray();
					bw.write(content);
        			bw.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
        			
            	}
            	mainFile = new File(dirname + "/" + username + "/records");
        		mainFile.mkdir();
                returnString = "Success";
            } else {
                returnString = "Could not create .../users/" + username;
            }
        } else {
        	returnString = "This username already exists.";
        }
		
		return returnString;
	}
	
	public String getContentFromFile(String username, String file){
		
		BufferedReader br = null;
		String stringToReturn = "";

		try {

			String line;

			br = new BufferedReader(new FileReader(dirname + "/" + username + "/" + file));

			while ((line = br.readLine()) != null) {
				stringToReturn += line + "'";
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		stringToReturn = stringToReturn.substring(0, stringToReturn.length() - 1);
		
		return stringToReturn;
	}
	
	public void overwrite(String username, String fileName, ArrayList<String> newTextARRAYLIST){
		
		DataOutputStream outstream;
		String newTextSTRING = "";
		
		for (int i = 0; i < newTextARRAYLIST.size(); i++){
			newTextSTRING += newTextARRAYLIST.get(i) + "\n";
		}
				
		newTextSTRING = newTextSTRING.substring(0, newTextSTRING.length() - 1);
			
		File file = new File(dirname + "/" + username + "/" + fileName + ".txt");
		
		try {
			outstream = new DataOutputStream(new FileOutputStream(file,false));
			outstream.write(newTextSTRING.getBytes());
			outstream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} 
		
	}
	
	private String getSize(File file) {
	 File[] listOfFiles = file.listFiles();
	 int count = 1;
	 for (int i = 0; i < listOfFiles.length; i++){
		 if (listOfFiles[i].isDirectory()){
			 if (!listOfFiles[i].getName().equals(".D_Store")){
				 count++;
			 }
		 }
	 }
		
	    return Long.toString(count);
	}
	
}
