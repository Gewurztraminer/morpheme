package Morpheme;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

public class MorphemeClientThread extends Thread {
	
	private Socket socket;
	public BufferedReader br;
	public BufferedWriter bw;
	
	public static boolean thisClientIsDead = false;
	
	public int dictionary;
	public int percentageOfAccepted;
	public int boardDimensions;
	public int minimumWordLength;
	public int minimumWordCount;
	public Integer[] freq;
	public int bonusWordGreaterThan;
	public int bonusWordLessThan;
	
	int ID = 0;
	int lol = 0;
	
	public MorphemeClientThread(Socket socket){
		
		this.socket = socket;
				
	}
	
	public void run(){		
							
			try {
				bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
				br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				mainActivity();
			} catch (IOException e) {
				e.printStackTrace();
			}
				        
	}
	
	private void mainActivity(){
		
		
		while (true){
			
			String inputString = "";
			boolean thereIsInput = false;

			try {
			
					while (br.ready()){
						int character = (int) br.read(); 
						if (character == -1){
							send("close");
							thisClientIsDead = true;
							break;
						}
						inputString = inputString + (char) character;
						thereIsInput = true;
					}
										
					if (thereIsInput){
						
						ArrayList<Object> parsedInputARRAYLIST = parseStringInputToArrayList(inputString);
						String identifier = (String) parsedInputARRAYLIST.get(0);
						parsedInputARRAYLIST.remove(0);
						 
							 if (identifier.equals("load board")){
																
									dictionary = (Integer) parsedInputARRAYLIST.get(0);
									percentageOfAccepted = (Integer) parsedInputARRAYLIST.get(1);
									boardDimensions = (Integer) parsedInputARRAYLIST.get(2);
									minimumWordLength = (Integer) parsedInputARRAYLIST.get(3);
									minimumWordCount = (Integer) parsedInputARRAYLIST.get(4);
									bonusWordGreaterThan = (Integer) parsedInputARRAYLIST.get(5);
									bonusWordLessThan = (Integer) parsedInputARRAYLIST.get(6);
									freq = (Integer[]) parsedInputARRAYLIST.get(7);
		
									loadBoard();
							 }
							 
							 if (identifier.equals("create user")){
						
							 }
							 
							 if (identifier.equals("terminate server")){
									
								System.exit(0);
								
							 }
						 
					}
					
			} catch (IOException e) {
				thisClientIsDead = true;
				close();
			}								
	

	
		}
}
				



	
	public void loadBoard(){
		
		String[][] board;
		ArrayList<String> wordsInBoard;
		BonusWord bw = new BonusWord(bonusWordGreaterThan, bonusWordLessThan, dictionary, boardDimensions, minimumWordLength);
		String randomWord = bw.getRandomWord();
		int wordCountInBoardExcludingPersonalWords;		
		
		long startTime = System.currentTimeMillis();
	
		do {
	
		wordCountInBoardExcludingPersonalWords = 0;
		board = new LoadBoard().getBoard(boardDimensions, freq);
		bw.embedRandomWord(randomWord, board);
		wordsInBoard = new SolverList(board, MorphemeServer.dictWordHashTables.get(dictionary), MorphemeServer.potentialWordHashTables.get(dictionary), minimumWordLength);
		
		if (wordsInBoard.size() < minimumWordCount) continue;
		
		long endTime   = System.currentTimeMillis();
		if (((endTime - startTime) / 1000) >= 10){
			randomWord = bw.getRandomWord();
			startTime = System.currentTimeMillis();
			wordsInBoard.clear();
			continue;
		}

		
		//if (isBoardUnique(wordsInBoard) == false){
		//	wordsInBoard.clear();
		//	continue;
		//}
				
		if (dictionary == 0 && MorphemeServer.canWeUsePersonalizedWordList == true){
		for (int v = 0; v < wordsInBoard.size(); v++){
			if (!MorphemeServer.personalWords.contains(wordsInBoard.get(v))){
			wordCountInBoardExcludingPersonalWords++;
			}
		}
		} else {
			wordCountInBoardExcludingPersonalWords = wordsInBoard.size();
		}
		
		} while (wordCountInBoardExcludingPersonalWords < minimumWordCount);
		
		Collections.sort(wordsInBoard, new OrderingListByLength());
		
		String g = "";
		for (int k = 0; k < boardDimensions; k++){
			String b = "";
			for (int l = 0; l < boardDimensions; l++){
				b = b + board[k][l] + " ";
			}	
			
			g = g + b + "\n";
		}
		
		long endTime   = System.currentTimeMillis();
		long totalTime = (endTime - startTime) / 1000;
		System.out.println(g);
		System.out.println(wordsInBoard);
		System.out.println(totalTime);

		
		String lettersForBoardStringToPassThrough = "";
		
		for (int u = 0; u < boardDimensions; u++){
			for (int h = 0; h < boardDimensions; h++){
				lettersForBoardStringToPassThrough = lettersForBoardStringToPassThrough + board[u][h];
			}
		}
		
		String[] wordsInBoardConverted = convertStringArrayListIntoArray(wordsInBoard);

		Object[] parametersArray = { "load board", lettersForBoardStringToPassThrough, randomWord, wordsInBoardConverted };

		ArrayList<Object> boardParticulars = addElementsFrom(parametersArray);
		
		String sendableString = convertToSendableString(boardParticulars);
				
		send(sendableString);

	}
	
	public boolean isBoardUnique(ArrayList<String> wordsInBoard){
		
		boolean torf = false;
		
		if (wordsInBoard.size() > 0){
					
		double good = (howManyWordsInArrayInFile(wordsInBoard, "v6"));
		good = good + (howManyWordsInArrayInFile(wordsInBoard, "g6"));
		good = good + (howManyWordsInArrayInFile(wordsInBoard, "v7"));
		good = good + (howManyWordsInArrayInFile(wordsInBoard, "g7"));
		double bad = (howManyWordsInArrayInFile(wordsInBoard, "h6"));
		bad = bad + (howManyWordsInArrayInFile(wordsInBoard, "n6"));
		bad = bad + (howManyWordsInArrayInFile(wordsInBoard, "h7"));
		bad = bad + (howManyWordsInArrayInFile(wordsInBoard, "n7"));
		double total = (double) (good + bad);
		double percentDouble = (double) (good / total) * 100;
		int percentInt = Integer.parseInt(String.format("%.0f", percentDouble));
		
		if (total != 0 && percentInt >= percentageOfAccepted){
			torf = true;
		}
		
		}
		
		return torf;
	}
	
	public ArrayList<Object> parseStringInputToArrayList(String message){
		ArrayList<Object> input = new ArrayList<Object>();
		System.out.println("parseString..." + message);
		String[] splittedString = message.split("~`~");
		for (int i = 0; i < splittedString.length; i++){
			try{
				char c = splittedString[i].charAt(0);
				splittedString[i] = splittedString[i].substring(1, splittedString[i].length());
				
				if (c == 's'){
					input.add(splittedString[i]);
				}
				
				if (c == 'i'){
					input.add(Integer.parseInt(splittedString[i]));
				}
				
				if (c == 'S'){
					String[] stringArray = splittedString[i].split("`");
					input.add(stringArray);
				}

				if (c == 'I'){
					String[] integerStringArray = splittedString[i].split("`");
					Integer[] integerArray = new Integer[integerStringArray.length];
					for (int j = 0; j < integerStringArray.length; j++){
						integerArray[j] = Integer.parseInt(integerStringArray[j]);
					}
					input.add(integerArray);
				}


			} catch (Exception e){
				System.out.println("Output must be either String, Integer, or an array.");
				e.printStackTrace();
			}
		}
			
		return input;
	}
	
	
	public int howManyWordsInArrayInFile(ArrayList<String> wordsInBoard, String type){
	
		int number = 0;
		Hashtable<String, Integer> hashInQuestion = null;
		
		if (type.equals("v7")) hashInQuestion = MorphemeServer.v7;
		if (type.equals("g7")) hashInQuestion = MorphemeServer.g7;
		if (type.equals("h7")) hashInQuestion = MorphemeServer.h7; 
		if (type.equals("n7")) hashInQuestion = MorphemeServer.n7;
		if (type.equals("v6")) hashInQuestion = MorphemeServer.v6;
		if (type.equals("g6")) hashInQuestion = MorphemeServer.g6;
		if (type.equals("h6")) hashInQuestion = MorphemeServer.h6;
		if (type.equals("n6")) hashInQuestion = MorphemeServer.n6;
		
		for (int f = 0; f < wordsInBoard.size(); f++){
			if (hashInQuestion.containsKey(wordsInBoard.get(f))){
				number++;
			}
		}
		
		return number;
	}

	private ArrayList<Object> addElementsFrom(Object[] elements){
		
		ArrayList array = new ArrayList();
		
		for (int h = 0; h < elements.length; h++){
			array.add(elements[h]);
		}
		
		return array;
	}
	
	
	private String convertToSendableString(Object message){
		String stringToSend = "";
		
		if (message instanceof ArrayList){
			for (int i = 0; i < ((ArrayList<Object>) message).size(); i++){
				if (((ArrayList<Object>) message).get(i) instanceof String){
					stringToSend += "s" + ((ArrayList<Object>) message).get(i);
				}
				if (((ArrayList<Object>) message).get(i) instanceof Integer){
					stringToSend += "i" + Integer.toString((Integer)((((ArrayList<Object>) message).get(i))));
				}
				if (((ArrayList<Object>) message).get(i).getClass().isArray()){

					if (((ArrayList<Object>) message).get(i) instanceof String[]){
						stringToSend += "S";
						String[] stringArray = (String[])(((ArrayList<Object>) message).get(i));
						for (int j = 0; j < ((String[]) ((ArrayList<Object>) message).get(i)).length; j++){
							
							if (j != ((String[]) ((ArrayList<Object>) message).get(i)).length - 1){
							stringToSend += stringArray[j] + "`";
							} else {
								stringToSend += stringArray[j];
							}
						}
					}
					if (((ArrayList<Object>) message).get(i) instanceof Integer[]){
						stringToSend += "I";
						Integer[] integerArray = (Integer[])(((ArrayList<Object>) message).get(i));
						for (int j = 0; j < ((Integer[]) ((ArrayList<Object>) message).get(i)).length; j++){
							if (j != ((Integer[]) ((ArrayList<Object>) message).get(i)).length - 1){
							stringToSend += Integer.toString(integerArray[j]) + "`";
							} else {
								stringToSend += Integer.toString(integerArray[j]);
							}
						}
					}
				}
				stringToSend += "~`~";
			}
		}
		
		if (message.getClass().isArray()){
			
		}
		
		return stringToSend;
	}
	
	public String[] convertStringArrayListIntoArray(ArrayList<String> arrayList){
		String[] array = new String[arrayList.size()];
		
		for (int i = 0; i < arrayList.size(); i++){
			array[i] = arrayList.get(i);
		}
		
		return array;
	}
	
	public void send(String message){
		
		try {
			bw.write(message);
			bw.flush();
		} catch (Exception e) {
			close();
		}
		
	}

	private void close(){
	
		if (MorphemeServer.threads.contains(this)){
		
		try {
			br.close();		
			bw.close();
			socket.close();
			MorphemeServer.threads.remove(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		}
		
	}

	
}
