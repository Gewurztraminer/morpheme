package Morpheme;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class MorphemeClient {
	
	public int dictionary = 0;
	public int percentageOfAccepted = 66;
	public int boardDimensions = 6;
	public int minimumWordLength = 6;
	public int minimumWordCount = 200;
	public int bonusWordGreaterThanOrEqualTo = 4;
	public int bonusWordLessThanOrEqualTo = 10;
	public Integer[] freq = {1000, 319, 439, 300, 600, 208, 230, 333, 900, 28, 194, 540, 388, 530, 760, 402, 28, 650, 480, 540, 475, 139, 166, 41, 250, 55}; 
	
	public int preferredSizeOfLetters = 300;
	public int imageSizeInPixels = preferredSizeOfLetters / boardDimensions;
	
	public ByteArrayOutputStream baos = new ByteArrayOutputStream();
	public BufferedReader br;
	public BufferedWriter bw;
	public Socket socket;
	
	JFrame jf;
	JTextArea f;
	JTextArea ff;

public static void main(String[] args){
	new MorphemeClient();
}

public MorphemeClient() {
	
	setUpSocketAndStreams();
	
	
	
	jf = new JFrame();
	jf.setBounds(0,0,500,500);
    jf.getContentPane().add(deleteAfter());
	jf.setVisible(true);
	jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	new Timer().schedule(
		    new TimerTask() {

		        @Override
		        public void run() {
		          mainActivity();
		        }
		        
		    }, 0, 100);
       
    }

public JPanel deleteAfter(){
	
	JPanel jp = new JPanel(new BorderLayout());
	
	JButton button = new JButton("Load Board");
	JButton terminateServer = new JButton("Terminate Server");
	JPanel fff = new JPanel(new BorderLayout());
	f = new JTextArea();
	f.setEditable(false);
	Font font = new Font(Font.MONOSPACED, Font.BOLD, 55);
	f.setFont(font);
	ff = new JTextArea();
	fff.add(f, BorderLayout.NORTH);
	fff.add(new JScrollPane(ff), BorderLayout.CENTER);
    jp.add(button, BorderLayout.NORTH);
    jp.add(fff, BorderLayout.CENTER);
    jp.add(terminateServer, BorderLayout.SOUTH);
		
	button.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent d){
			
			Object[] parametersArray = { "load board", dictionary, percentageOfAccepted, boardDimensions, minimumWordLength, minimumWordCount, bonusWordGreaterThanOrEqualTo, bonusWordLessThanOrEqualTo, freq };

			ArrayList<Object> parameters = addElementsFrom(parametersArray);
			
			send(parameters);
			
		}
	});

	
	terminateServer.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent d){
			
			send("sterminate server~`~");
			
		}
	});
    
    jp.setVisible(true);
	jp.setOpaque(true);
	return jp;
	
}


public void mainActivity(){
	
	
	if (socket.isConnected()){
				
			String inputString = "";
			boolean thereIsInput = false;

			try {
			
				
				while (br.ready()){
					int character = (int) br.read(); 
					if (character == -1){
						send("sclose~`~");
						close();
						break;
					}
					inputString = inputString + (char) character;
					thereIsInput = true;
				}
				
				if (thereIsInput){
					
				ArrayList<Object> parsedInputARRAYLIST = parseStringInputToArrayList(inputString);
				String identifier = (String) parsedInputARRAYLIST.get(0);
				parsedInputARRAYLIST.remove(0);
			
				
				 if (identifier.equals("load board")){
					 					 
					 String w = (String) parsedInputARRAYLIST.get(0);
					 String y = "";
					 for (int b = 0; b < boardDimensions; b++){
						 for (int c = 0; c < boardDimensions; c++){
							 y = y + w.substring((b * boardDimensions) + c, (b * boardDimensions) + c + 1) + " ";
						 }
						 if (b != boardDimensions - 1){
						 y = y + "\n";
						 }
					 }
					 System.out.println(parsedInputARRAYLIST);
					 String[] c = (String[]) parsedInputARRAYLIST.get(2);
					 String e = "";
					 for (int v = 0; v < c.length; v++){
						 e = e + c[v] + "\n";
					 }
					 
					 f.setText(y);
					 ff.setText(e);
					 
					}
				 
				 if (identifier.equals("close")){
					 close();
				 }
		


	

		/**
	Object ob;
	try {
		ob = ((ObjectInputStream) is).readObject();
		
		if (ob instanceof ArrayList<?>){
			
			ArrayList g = (ArrayList) ob;
			String gg = (String) g.get(0);
			 
				 if (gg.equals("load board")){
					 
					 g.remove(0);
					 
					 String w = (String) g.get(0);
					 String y = "";
					 for (int b = 0; b < boardDimensions; b++){
						 for (int c = 0; c < boardDimensions; c++){
							 y = y + w.substring((b * boardDimensions) + c, (b * boardDimensions) + c + 1) + " ";
						 }
						 if (b != boardDimensions - 1){
						 y = y + "\n";
						 }
					 }
					 
					 ArrayList<String> c = (ArrayList<String>) g.get(2);
					 String e = "";
					 for (int v = 0; v < c.size(); v++){
						 e = e + c.get(v) + "\n";
					 }
					 
					 f.setText(y);
					 ff.setText(e);
				 }
				 
			
		}
		
		if (ob instanceof String){
			
			String message = (String) ob;
			
		}
	} catch (ClassNotFoundException e) {
		close();
	}
	catch (IOException e) {
		close();
	}
	} else {
		close();
	}
	**/
				}
			} catch (IOException e) {
				e.printStackTrace();
			}	
	} 
	
}

public void send(Object message){
	String stringToSend = convertToSendableString(message);
	try {
		bw.write(stringToSend);
		bw.flush();
	} catch (Exception e) {
		close();
	}	
	
}

	
	private String convertToSendableString(Object message){
		String stringToSend = "";
		
		
		if (message instanceof ArrayList){
			for (int i = 0; i < ((ArrayList<Object>) message).size(); i++){
				System.out.println(((ArrayList<Object>) message).get(i));
				if (((ArrayList<Object>) message).get(i) instanceof String){
					stringToSend += "s" + ((ArrayList<Object>) message).get(i);
				}
				if (((ArrayList<Object>) message).get(i) instanceof Integer){
					stringToSend += "i" + Integer.toString((Integer)((((ArrayList<Object>) message).get(i))));
				}
				if (((ArrayList<Object>) message).get(i).getClass().isArray()){

					if (((ArrayList<Object>) message).get(i) instanceof String[]){
						stringToSend += "S";
						String[] stringArray = (String[])(((ArrayList<Object>) message).get(i));
						for (int j = 0; j < ((String[]) ((ArrayList<Object>) message).get(i)).length; j++){
							if (j != ((String[]) ((ArrayList<Object>) message).get(i)).length - 1){
							stringToSend += stringArray[j] + "`";
							} else {
								stringToSend += stringArray[j];
							}
						}
					}
					if (((ArrayList<Object>) message).get(i) instanceof Integer[]){
						stringToSend += "I";
						Integer[] integerArray = (Integer[])(((ArrayList<Object>) message).get(i));
						for (int j = 0; j < ((Integer[]) ((ArrayList<Object>) message).get(i)).length; j++){
							if (j != ((Integer[]) ((ArrayList<Object>) message).get(i)).length){
							stringToSend += Integer.toString(integerArray[j]) + "`";
							} else {
								stringToSend += Integer.toString(integerArray[j]);
							}
						}
					}
				}
				stringToSend += "~`~";
			}
		}
		
		
		if (message.getClass().isArray()){
			
		}
		
		if (message instanceof String){
			
			stringToSend = (String) message;

		}
		System.out.println(stringToSend);
		return stringToSend;
	}
	
	public ArrayList<Object> parseStringInputToArrayList(String message){
		ArrayList<Object> input = new ArrayList<Object>();
		
		String[] splittedString = message.split("~`~");
		for (int i = 0; i < splittedString.length; i++){
			try{
				char c = splittedString[i].charAt(0);
				splittedString[i] = splittedString[i].substring(1, splittedString[i].length());
				
				if (c == 's'){
					input.add(splittedString[i]);
				}
				
				if (c == 'i'){
					input.add(Integer.parseInt(splittedString[i]));
				}
				
				if (c == 'S'){
					String[] stringArray = splittedString[i].split("`");
					input.add(stringArray);
				}

				if (c == 'I'){
					String[] integerStringArray = splittedString[i].split("`");
					Integer[] integerArray = new Integer[integerStringArray.length];
					for (int j = 0; j < integerStringArray.length; j++){
						integerArray[j] = Integer.parseInt(integerStringArray[j]);
					}
					input.add(integerArray);
				}


			} catch (Exception e){
				System.out.println("Output must be either String, Integer, or an array.");
				e.printStackTrace();
			}
		}
		
		return input;
	}
	
	private void setUpSocketAndStreams(){
			
		try {
			//52.34.206.59
			socket = new Socket(InetAddress.getByName("127.0.0.1"), 6780);
		} catch (UnknownHostException e){
			
			try {
				socket.close();
			} catch (IOException io){
				io.printStackTrace();
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException io){
			
			try {
				br.close();
				bw.close();
				socket.close();
			} catch (IOException i){
				i.printStackTrace();
			}
			
		}
	}

private ArrayList<Object> addElementsFrom(Object[] elements){
	
	ArrayList array = new ArrayList();
	
	for (int h = 0; h < elements.length; h++){
		array.add(elements[h]);
	}
	
	return array;
}
	
	private void close(){
		
		try{
			br.close();
			bw.close();
			socket.close();
			
			JOptionPane.showMessageDialog(jf, "The server is not responding at the moment. Some changes are probably being made.\n\nTry again in a few minutes.");
			System.exit(0);
		} catch (IOException io){
			io.printStackTrace();
		}
	}
	
	
}
	
